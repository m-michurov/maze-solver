#include <iostream>
#include <cmath>
#include <fstream>
#include <functional>
#include <memory>
#include <iterator>


#include "surface/surface_types/planar/planar_surface.hpp"
#include "surface/surface_types/toroid/toroid_surface.hpp"
#include "surface/surface_types/cylinder/cylinder_surface.hpp"
#include "search_algorithm/search_algorithm.hpp"
#include "surface/distance/distance.hpp"
#include "utility/arg_parser/parser.hpp"
#include "surface/presenter/presenter.hpp"
#include "utility/input_reader/surface_reader.hpp"
#include "utility/strings/utility.hpp"


static inline void printHelp()
{
    static char const *help =
            R"=(Usage: solver [KEYS]
Finds the shortest possible path in a maze depending on surface topology.

Allowed keys:
    -p, --planar               set surface topology to planar

    -c, --cylinder             set surface topology to cylinder
                               (allowed to jump from left border to right
                               border and vice versa)

    -t, --tor                  set surface topology to tor
                               (allowed to jump from left border to right
                               border, form top to bottom and vice versa)

    -i, --in=IN_FILE_NAME      (required) read surface from IN_FILE_NAME

    -o, --out=OUT_FILE_NAME    write output to OUT_FILE_NAME

    -C, --convert              convert input form text to bmp or vice versa depending on
                               file extensions

    -P, --path                 write path to "{OUT_FILE_NAME}.path.txt"
                               (origin is top left corner)

    -I, --insensitive          treat wider range of colours as black/white
                               (if input file is a .bmp image)

    -o, --out=OUT_FILE_NAME    write output to OUT_FILE_NAME (write to stdout by default)

    -h, --help                 display help (the one you're reading right now))=";

    std::cout << help << std::endl;
}


static inline void writeResults(
        arg_parser::surface::GlobalArgs_t const & global_args,
        std::vector<surface::Point_t> const & path,
        Presenter const & presenter)
{
    if (endsWith(global_args.out_file, ".bmp"))
    {
        presenter.dump(global_args.out_file, Presenter::FileType_t::BMP, global_args.show_visited);
    }
    else if (endsWith(global_args.out_file, ".txt"))
    {
        presenter.dump(global_args.out_file, Presenter::FileType_t::Text, global_args.show_visited);
    }
    else
    {
        presenter.dump(global_args.out_file, Presenter::FileType_t::Text, global_args.show_visited);
        presenter.dump(global_args.out_file, Presenter::FileType_t::BMP, global_args.show_visited);
    }

    if (global_args.path)
    {
        std::ofstream out_file(global_args.out_file + ".path.txt");

        if (out_file.fail())
        {
            std::cout << "Error: unable to open output file \"" << global_args.out_file + ".path.txt" << "\""
                      << std::endl;
            exit(EXIT_FAILURE);
        }

        if (path.empty())
        {
            out_file << "No path" << std::endl;
        }
        else
        {
            std::ostream_iterator<surface::Point_t> out_it(out_file, "\n");

            std::copy(path.begin(), path.end(), out_it);
        }
    }
}


static inline void convert(
        std::string const & filename,
        Presenter const & presenter)
{
    if (endsWith(filename, ".bmp"))
    {
        presenter.dump(filename.substr(0, filename.length() - 4) + ".txt", Presenter::FileType_t::Text, false);
    }
    else if (endsWith(filename, ".txt"))
    {
        presenter.dump(filename.substr(0, filename.length() - 4) + ".bmp", Presenter::FileType_t::BMP, false);
    }
    else
    {
        presenter.dump(filename + ".bmp", Presenter::FileType_t::BMP, false);
    }
}


int main(
        int argc,
        char *argv[])
{
    std::vector<std::string> surface_lines;

    arg_parser::surface::GlobalArgs_t global_args;

    try
    {
        global_args = arg_parser::surface::parse_arguments(argc, argv);
    }
    catch (std::runtime_error & e)
    {
        std::cout << "Error: " << e.what() << std::endl << std::endl;

        printHelp();

        exit(EXIT_FAILURE);
    }

    if (global_args.help)
    {
        printHelp();
        // exit(EXIT_SUCCESS);
    }

    try
    {
        surface_lines = read_surface(global_args);
    }
    catch (std::runtime_error & e)
    {
        std::cout << "Error: " << e.what() << std::endl << std::endl;

        printHelp();

        exit(EXIT_FAILURE);
    }

    std::vector<surface::Point_t> path;

    std::unique_ptr<ISurface> surface;
    std::function<size_t(surface::Point_t const &)> distance;

    int height = surface_lines.size();
    int width = surface_lines[0].size();

    surface::Point_t start = { -1, -1 };
    surface::Point_t finish = { -1, -1 };

    switch (global_args.topology)
    {
        case surface::Topology_t::Planar:
            try
            {
                auto surface_p = std::make_unique<surface::PlanarSurface>(surface_lines);
                start = surface_p->start();
                finish = surface_p->finish();

                distance = surface::distance::makeDistanceFunction<surface::PlanarSurface>(finish);
                surface = std::move(surface_p);
            }
            catch (std::runtime_error & e)
            {
                std::cout << "Error: " << e.what() << std::endl;
                exit(EXIT_FAILURE);
            }
            break;
        case surface::Topology_t::Cylinder:
            try
            {
                auto surface_p = std::make_unique<surface::CylinderSurface>(surface_lines);
                start = surface_p->start();
                finish = surface_p->finish();

                distance = surface::distance::makeDistanceFunction<surface::CylinderSurface>(finish, width);
                surface = std::move(surface_p);
            }
            catch (std::runtime_error & e)
            {
                std::cout << "Error: " << e.what() << std::endl;
                exit(EXIT_FAILURE);
            }
            break;
        case surface::Topology_t::Toroid:
            try
            {
                auto surface_p = std::make_unique<surface::ToroidSurface>(surface_lines);
                start = surface_p->start();
                finish = surface_p->finish();

                distance = surface::distance::makeDistanceFunction<surface::ToroidSurface>(finish, width, height);
                surface = std::move(surface_p);
            }
            catch (std::runtime_error & e)
            {
                std::cout << "Error: " << e.what() << std::endl;
                exit(EXIT_FAILURE);
            }
            break;
    }

    Presenter presenter(*surface);

    if (global_args.convert)
    {
        convert(global_args.in_file, presenter);
    }

    path = findPath(
            *surface,
            start,
            finish,
            distance,
            presenter
    );

    writeResults(global_args, path, presenter);

    exit(EXIT_SUCCESS);
}
