#pragma once


#include <string>


bool endsWith(
        std::string const & str,
        std::string const & suffix);
