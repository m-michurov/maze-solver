#include <iterator>


#include "reader.hpp"


std::vector<std::string> input_reader::txt::read(std::ifstream & is)
{
    std::vector<std::string> result;

    std::istream_iterator<std::string> in_it(is);

    std::copy(in_it, std::istream_iterator<std::string>(), std::back_inserter<std::vector<std::string>>(result));

    return result;
}
