#pragma once


#include <vector>
#include <string>


#include "utility/bmp/bmp.hpp"


namespace input_reader
{
    namespace txt
    {
        std::vector<std::string> read(std::ifstream &);
    }
}
