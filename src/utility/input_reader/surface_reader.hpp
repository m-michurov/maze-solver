#pragma once


#include <vector>
#include <string>


#include "utility/arg_parser/parser.hpp"


std::vector<std::string> read_surface(arg_parser::surface::GlobalArgs_t const & global_args);
