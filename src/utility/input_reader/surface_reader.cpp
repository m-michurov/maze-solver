#include "surface_reader.hpp"


#include "utility/strings//utility.hpp"
#include "utility/bmp/bmp.hpp"
#include "utility/input_reader/bmp_reader/reader.hpp"
#include "utility/input_reader/txt_reader/reader.hpp"


std::vector<std::string> read_surface(arg_parser::surface::GlobalArgs_t const & global_args)
{
    std::vector<std::string> surface_lines;

    if (endsWith(global_args.in_file, ".bmp"))
    {
        BMP bmp_file(1, 1);

        try
        {
            bmp_file.read(global_args.in_file);

            surface_lines = input_reader::bmp::read(bmp_file, global_args.insensitive);
        }
        catch (...)
        {
            throw std::runtime_error("Unable to open input file \"" + global_args.in_file + "\"");
        }
    }
    else
    {
        std::ifstream in_file;
        in_file.open(global_args.in_file);

        if (in_file.fail())
        {
            throw std::runtime_error("Unable to open input file \"" + global_args.in_file + "\"");
        }

        surface_lines = input_reader::txt::read(in_file);
    }

    return surface_lines;
}
