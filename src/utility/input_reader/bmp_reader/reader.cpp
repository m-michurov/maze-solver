#include "reader.hpp"


uint8_t convertToGrayscale(Pixel_t const & pixel)
{
    return (0.299 * pixel.R + 0.587 * pixel.G + 0.114 * pixel.B);
}


bool singleColour(Pixel_t const & pixel)
{
    return (pixel.R > 63 and pixel.G == 0 and pixel.B == 0)
           or (pixel.R == 0 and pixel.G > 63 and pixel.B == 0)
              or (pixel.R == 0 and pixel.G == 0 and pixel.B > 63);
}


std::vector<std::string> input_reader::bmp::read(
        BMP const &bmp_file,
        bool insensitive)
{
    int32_t width = bmp_file.bmp_info_header.width;
    int32_t height = bmp_file.bmp_info_header.height;

    std::vector<std::string> result(height, std::string(width, '.'));

    char character = '.';

    Pixel_t current = {};
    Pixel_t const black = {0, 0, 0, 255};
    Pixel_t const white = {255, 255, 255, 255};
    uint8_t gs = 0;

    bool is_sf = false;
    bool start = false;

    for (int32_t y = 0; y < height; ++y)
    {
        for (int32_t x = 0; x < width; ++x)
        {
            current = bmp_file.at(x, y);

            if (insensitive)
            {
                gs = convertToGrayscale(current);
                is_sf = singleColour(current);
            }

            if ((not insensitive and current == black) or (insensitive and gs < 64 and not is_sf))
            {
                character = '#';
            }
            else if ((not insensitive and current == white) or (insensitive and gs >= 64 and not is_sf))
            {
                character = '.';
            }
            else
            {
                // std::cout << "SF" << std::endl;
                if (not start)
                {
                    start = true;
                    character = 'S';
                }
                else
                {
                    character = 'F';
                }
            }

            result[height - 1 - y][x] = character;
        }
    }

    return result;
}
