#pragma once


#include <vector>
#include <string>


#include "utility/bmp/bmp.hpp"


namespace input_reader
{
    namespace bmp
    {
        std::vector<std::string> read(
                BMP const &bmp_file,
                bool insensitive = false);
    }
}
