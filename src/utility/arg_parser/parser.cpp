#include <getopt.h>


#include "parser.hpp"


static option long_options[] = {
        {"planar",      no_argument,       nullptr, 'p'},
        {"cylinder",    no_argument,       nullptr, 'c'},
        {"tor",         no_argument,       nullptr, 't'},
        {"path",        no_argument,       nullptr, 'P'},
        {"visited",     no_argument,       nullptr, 'v'},
        {"convert",     no_argument,       nullptr, 'C'},
        {"insensitive", no_argument,       nullptr, 'I'},
        {"in",          required_argument, nullptr, 'i'},
        {"out",         required_argument, nullptr, 'o'},
        {"help",        no_argument,       nullptr, 'h'},
        {nullptr,       0,                 nullptr, 0}
};


arg_parser::surface::GlobalArgs_t arg_parser::surface::parse_arguments(
        int argc,
        char **argv) {
    GlobalArgs_t global_args = {::surface::Topology_t::Planar, "", "a.out", false, false, false, false};

    char const *opt_string = "-:PCIpcthvi:o:";
    int arg;

    bool found_topology = false;

    opterr = 0;

    while ((arg = getopt_long(argc, argv, opt_string, long_options, nullptr)) != -1) {
        switch (arg) {
            case 'p':
                if (not found_topology) {
                    global_args.topology = ::surface::Topology_t::Planar;
                    found_topology = true;
                } else {
                    throw std::runtime_error("Bad argument count: only one topology can be specified");
                }
                break;
            case 'c':
                if (not found_topology) {
                    global_args.topology = ::surface::Topology_t::Cylinder;
                    found_topology = true;
                } else {
                    throw std::runtime_error("Bad argument count: only one topology can be specified");
                }
                break;
            case 'P':
                global_args.path = true;
                break;
            case 'C':
                global_args.convert = true;
                break;
            case 'I':
                global_args.insensitive = true;
                break;
            case 't':
                if (not found_topology) {
                    global_args.topology = ::surface::Topology_t::Toroid;
                    found_topology = true;
                } else {
                    throw std::runtime_error("Bad argument count: only one topology can be specified");
                }
                break;
            case 'i':
                global_args.in_file = optarg;
                break;
            case 'v':
                global_args.show_visited = true;
                break;
            case 'o':
                global_args.out_file = optarg;
                break;
            case 'h':
                global_args.help = true;
                break;
            case ':':
                throw std::runtime_error(std::string(argv[optind - 1]) + " is missing argument");
            case '?':
                throw std::runtime_error("Unknown option");
            case 1:
                throw std::runtime_error(
                        "Non-option encountered: " + std::string(argv[optind - 1])
                        + "\n(this program takes no non-option arguments)"
                );
            default:
                throw std::runtime_error("Bad option: " + std::string(argv[optind - 1]));
        }
    }

    if (global_args.in_file.empty()) {
        throw std::runtime_error("No input file specified");
    }

    return global_args;
}
