#ifndef MAZE_PARSER_HPP
#define MAZE_PARSER_HPP


#include <iostream>


#include "surface/common.hpp"


namespace arg_parser
{
    namespace surface
    {
        struct GlobalArgs_t
        {
            ::surface::Topology_t topology;
            std::string in_file;
            std::string out_file;
            bool help;
            bool path;
            bool convert;
            bool insensitive;
            bool show_visited;
        };


        GlobalArgs_t parse_arguments(
                int argc,
                char ** argv);
    }
}


#endif //MAZE_PARSER_HPP
