#ifndef MAZESOLVER_MAP_HPP
#define MAZESOLVER_MAP_HPP


template <typename Key_t, typename Value_t>
class Map
{
public:
    explicit Map(Value_t const & default_value)
            :
            default_value(default_value)
    {}

    Value_t & operator[](Key_t const & key)
    {
        auto it = this->data.find(key);

        if (it == this->data.end())
        {
            Value_t & ref = this->data[key];
            ref = this->default_value;
            return ref;
        }

        return it->second;
    }

private:
    Value_t const default_value;
    std::unordered_map<Key_t, Value_t> data;
};


#endif //MAZESOLVER_MAP_HPP
