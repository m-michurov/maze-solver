#pragma once


#include <vector>


#include "common.hpp"


class ISurface
{
public:
    virtual std::vector<surface::Point_t> getNeighbours(surface::Point_t const & point) const = 0;

    virtual std::vector<std::string> repr() const = 0;
};
