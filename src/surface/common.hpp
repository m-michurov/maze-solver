#ifndef MAZESOLVER_COMMON_HPP
#define MAZESOLVER_COMMON_HPP


#include <vector>
#include <string>
#include <sstream>


namespace surface
{
    enum Cell_t : char
    {
        Start = 'S',
        Finish = 'F',
        EmptySpace = '.',
        Wall = '#'
    };


    enum class Topology_t
    {
        Planar,
        Cylinder,
        Toroid
    };


    struct Point_t
    {
        int x;
        int y;
    };


    struct SurfaceMetadata_t
    {
        bool valid;

        Point_t inception;
        Point_t destination;
    };


    SurfaceMetadata_t traverseSurface(
            std::vector<std::string> const & surface);


    bool operator<(
            Point_t const & p1,
            Point_t const & p2);


    bool operator!=(
            Point_t const & p1,
            Point_t const & p2);


    bool operator==(
            Point_t const & p1,
            Point_t const & p2);


    std::ostream & operator<<(
            std::ostream & os,
            Point_t const & point);
}


template <>
class std::hash<surface::Point_t>
{
public:
    size_t operator()(surface::Point_t const & point) const;
};


#endif //MAZESOLVER_COMMON_HPP
