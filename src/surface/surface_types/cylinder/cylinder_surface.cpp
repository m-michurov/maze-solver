#include <stdexcept>
#include <algorithm>
#include <random>


#include "cylinder_surface.hpp"


using namespace surface;


using Point = surface::Point_t;


CylinderSurface::CylinderSurface(
        std::vector<std::string> surface)
        :
        metadata(surface::traverseSurface(surface)),
        surface(std::move(surface))
{
    if (not this->metadata.valid)
    {
        throw std::runtime_error("Invalid surface");
    }
}


std::vector<Point> CylinderSurface::getNeighbours(
        Point const & point) const
{
    std::vector<Point> result;

    if (point.x - 1 > -1 and this->surface[point.y][point.x - 1] != Cell_t::Wall)
    {
        result.push_back({ point.x - 1, point.y });
    }
    else if (point.x - 1 == -1 and this->surface[point.y][this->surface[0].size() - 1] != Cell_t::Wall)
    {
        result.push_back({ static_cast<int>(this->surface[0].size() - 1), point.y });
    }

    if (point.x + 1 < this->surface[0].size() and this->surface[point.y][point.x + 1] != Cell_t::Wall)
    {
        result.push_back({ point.x + 1, point.y });
    }
    else if (point.x + 1 == this->surface[0].size() and this->surface[point.y][0] != Cell_t::Wall)
    {
        result.push_back({ 0, point.y });
    }

    if (point.y - 1 > -1 and this->surface[point.y - 1][point.x] != Cell_t::Wall)
    {
        result.push_back({ point.x, point.y - 1 });
    }
    if (point.y + 1 < this->surface.size() and this->surface[point.y + 1][point.x] != Cell_t::Wall)
    {
        result.push_back({ point.x, point.y + 1 });
    }

    return result;
}


Point const & CylinderSurface::start() const
{
    return this->metadata.inception;
}


Point const & CylinderSurface::finish() const
{
    return this->metadata.destination;
}


std::vector<std::string> CylinderSurface::repr() const
{
    return this->surface;
}
