#ifndef MAZE_CYLINDER_SURFACE_HPP
#define MAZE_CYLINDER_SURFACE_HPP


#include <string>
#include <iostream>


#include "surface/interface.hpp"
#include "surface/common.hpp"


namespace surface
{
    class CylinderSurface :
            public ISurface
    {
    public:
        typedef Point_t point_t;

        CylinderSurface() = delete;

        explicit CylinderSurface(std::vector<std::string> surface);

        std::vector<Point_t> getNeighbours(Point_t const & point) const override;

        Point_t const & start() const;

        Point_t const & finish() const;

        std::vector<std::string> repr() const override;

    private:
        SurfaceMetadata_t metadata;

        std::vector<std::string> surface;
    };
}


#endif //MAZE_CYLINDER_SURFACE_HPP
