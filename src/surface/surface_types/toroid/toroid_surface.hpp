#ifndef MAZE_TOROID_SURFACE_HPP
#define MAZE_TOROID_SURFACE_HPP


#include <string>
#include <iostream>


#include "surface/interface.hpp"
#include "surface/common.hpp"


namespace surface
{
    class ToroidSurface : public ISurface
    {
    public:
        typedef Point_t point_t;

        ToroidSurface() = delete;

        explicit ToroidSurface(std::vector<std::string> surface);

        std::vector<Point_t> getNeighbours(Point_t const & point) const override;

        Point_t const & start() const;

        Point_t const & finish() const;

        std::vector<std::string> repr() const override;

    private:
        SurfaceMetadata_t metadata;

        std::vector<std::string> surface;
    };
}


#endif //MAZE_TOROID_SURFACE_HPP
