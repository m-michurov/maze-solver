#include "common.hpp"


using namespace surface;


SurfaceMetadata_t surface::traverseSurface(
        std::vector<std::string> const & surface)
{
    if (surface.empty())
    {
        return {false, {-1, -1}, {-1, -1}};
    }

    size_t width = surface[0].size();

    SurfaceMetadata_t result = {
            true,
            {-1, -1},
            {-1, -1}
    };

    for (size_t row_index = 0; row_index < surface.size(); ++row_index)
    {
        auto row = surface[row_index];

        if (row.size() != width)
        {
            return {false, {-1, -1}, {-1, -1}};
        }

        for (size_t column_index = 0; column_index < width; ++column_index)
        {
            auto c = row[column_index];

            switch (c)
            {
                case Cell_t::Start:
                    if (result.inception.x != -1)
                    {
                        return {false, {-1, -1}, {-1, -1}};
                    }
                    result.inception = {static_cast<int>(column_index), static_cast<int>(row_index)};
                    break;
                case Cell_t::Finish:
                    if (result.destination.x != -1)
                    {
                        return {false, {-1, -1}, {-1, -1}};
                    }
                    result.destination = {static_cast<int>(column_index), static_cast<int>(row_index)};
                    break;
                case Cell_t::EmptySpace:
                case Cell_t::Wall:
                    break;
                default:
                    return {false, {-1, -1}, {-1, -1}};
            }
        }
    }

    if (result.destination.x > -1 and result.destination.y > -1 and result.inception.x > -1 and result.inception.y > -1)
    {
        result.valid = true;
        return result;
    }

    return {false, {-1, -1}, {-1, -1}};
}


bool surface::operator<(
        Point_t const & p1,
        Point_t const & p2)
{
    static_assert(sizeof(unsigned long long int) == 2 * sizeof(unsigned int), "");
    unsigned long long int i11 = *reinterpret_cast<unsigned int const *>(&p1.x);
    unsigned long long int i12 = *reinterpret_cast<unsigned int const *>(&p1.y);

    unsigned long long int i21 = *reinterpret_cast<unsigned int const *>(&p2.x);
    unsigned long long int i22 = *reinterpret_cast<unsigned int const *>(&p2.y);

    unsigned long long int p11 = (i11 << 32u) | i12;
    unsigned long long int p22 = (i21 << 32u) | i22;

    return p11 < p22;
}


bool surface::operator!=(
        Point_t const & p1,
        Point_t const & p2)
{
    return p1.x != p2.x or p1.y != p2.y;
}


bool surface::operator==(
        Point_t const & p1,
        Point_t const & p2)
{
    return not (p1 != p2);
}


std::ostream & surface::operator<<(
        std::ostream & os,
        Point_t const & point)
{
    os << point.x << ", " << point.y;

    return os;
}


size_t std::hash<Point_t>::operator()(surface::Point_t const & point) const
{
    unsigned long long int i1 = *reinterpret_cast<unsigned int const *>(&point.x);
    unsigned long long int i2 = *reinterpret_cast<unsigned int const *>(&point.y);

    return std::hash<unsigned long long int>()((i1 << 32u) | i2);
}
