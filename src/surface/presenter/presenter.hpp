#pragma once


#include <unordered_map>
#include <utility>
#include <vector>
#include <string>


#include "surface/common.hpp"
#include "surface/interface.hpp"


class Presenter
{
public:
    enum class PointState_t
    {
        Visited,
        Path,
        Start,
        End
    };

    enum class FileType_t
    {
        BMP,
        Text
    };

    explicit Presenter(ISurface const & surface);

    void update(
            surface::Point_t const & point,
            PointState_t state);

    void dump(
            std::string const &filename,
            FileType_t file_type,
            bool show_visited) const;

private:
    std::vector<std::string> repr;
};
