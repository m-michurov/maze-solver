#include <fstream>
#include <iterator>


#include "presenter.hpp"


#include "utility/bmp/bmp.hpp"
#include "utility/strings//utility.hpp"


void Presenter::dump(
        std::string const &filename,
        FileType_t file_type,
        bool show_visited) const
{
    int32_t width = this->repr[0].size();
    int32_t height = this->repr.size();

    if (file_type == FileType_t::BMP)
    {
        auto file = BMP(width, height);

        Pixel_t pixel = {};

        for (int32_t y = 0; y < height; ++y)
        {
            for (int32_t x = 0; x < width; ++x)
            {
                switch (this->repr[y][x])
                {
                    case '#':
                        pixel = { 0, 0, 0, 255 };
                        break;
                    case '+':
                        pixel = { 255, 0, 127, 255 };
                        break;
                    case 'S':
                    case 'F':
                        pixel = { 0, 0, 255, 255 };
                        break;
                    case '*':
                        if (show_visited)
                        {
                            pixel = { 32, 255, 127, 255 };
                            break;
                        }
                    default:
                        pixel = { 255, 255, 255, 255 };
                        break;
                }

                file.setPixel(x, height - 1 - y, pixel);
            }
        }

        if (not endsWith(filename, ".bmp"))
        {
            file.write(filename + ".bmp");
        }
        else
        {
            file.write(filename);
        }
    }
    else
    {
        auto file = endsWith(filename, ".txt") ? std::ofstream(filename) : std::ofstream(filename + ".txt");

        std::ostream_iterator<std::string> out_it(file, "\n");
        std::copy(this->repr.begin(), this->repr.end(), out_it);
    }
}


void Presenter::update(
        surface::Point_t const & point,
        Presenter::PointState_t state)
{
    char character = '.';

    switch (state)
    {
        case PointState_t::Visited:
            character = '*';
            break;
        case PointState_t::Path:
            character = '+';
            break;
        case PointState_t::Start:
            character = 'S';
            break;
        case PointState_t::End:
            character = 'F';
            break;
    }

    this->repr[point.y][point.x] = character;
}


Presenter::Presenter(ISurface const & surface) :
        repr(surface.repr())
{}
