#pragma once


#include "surface/interface.hpp"
#include "surface/presenter/presenter.hpp"


std::vector<surface::Point_t> findPath(
        ISurface const & surface,
        surface::Point_t const & inception,
        surface::Point_t const & destination,
        std::function<size_t(surface::Point_t const &)> h,
        Presenter & presenter);
