#include <unordered_set>
#include <vector>
#include <algorithm>
#include <functional>


#include "search_algorithm.hpp"


#include "utility/map/map.hpp"


using Point = surface::Point_t;


std::vector<Point> findPath(
        ISurface const & surface,
        Point const & inception,
        Point const & destination,
        std::function<size_t(Point const &)> h,
        Presenter & presenter)
{
    auto g = Map<Point, size_t>(std::numeric_limits<size_t>::max() / 2);

    g[inception] = 0;

    auto f = Map<Point, size_t>(std::numeric_limits<size_t>::max() / 2);

    f[inception] = h(inception);

    auto comp = [&f, &h](
            Point const & p1,
            Point const & p2) -> bool
    {
        if (f[p1] == f[p2])
        {
            return h(p1) < h(p2);
        }
        return f[p1] < f[p2];
    };

    auto open = std::unordered_set<Point>();
    open.insert(inception);

    auto parent = std::unordered_map<Point, Point>();
    parent[inception] = inception;

    while (not open.empty())
    {
        auto c = std::min_element(open.begin(), open.end(), comp);
        Point current = *c;

        open.erase(c);

        if (not(current == inception) and not(current == destination))
        {
            presenter.update(current, Presenter::PointState_t::Visited);
        }

        if (current == destination)
        {
            std::vector<Point> path;

            for (Point vertex = destination; parent[vertex] != vertex; vertex = parent[vertex])
            {
                path.push_back(vertex);

                presenter.update(vertex, Presenter::PointState_t::Path);
            }

            path.push_back(inception);

            std::reverse(path.begin(), path.end());

            presenter.update(inception, Presenter::PointState_t::Start);
            presenter.update(destination, Presenter::PointState_t::End);

            return path;
        }

        for (auto const & neighbour : surface.getNeighbours(current))
        {
            size_t tentative_gScore = g[current] + 1;

            if (tentative_gScore < g[neighbour])
            {
                parent[neighbour] = current;
                g[neighbour] = tentative_gScore;
                f[neighbour] = g[neighbour] + h(neighbour);

                if (open.find(neighbour) == open.end())
                {
                    open.insert(neighbour);
                }
            }
        }
    }

    return {};
}
