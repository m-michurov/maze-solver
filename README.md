# Maze Solver

A program that solves mazes on discrete surfaces

<img src="https://gitlab.com/m-michurov/maze-solver/raw/master/examples/in.bmp"  width="160" height="160"> -> <img src="https://gitlab.com/m-michurov/maze-solver/raw/master/examples/out.bmp"  width="160" height="160">

## Input format

Generally, a maze is represented as a binary matrix. 

#### Text

A text file consisting of **'.'**, **'?'**, **'S'** and **'F'**

* **'.'** - empty space
* **'#'** - wall
* **'S'** and **'F'** - start and end points (exactly one of each)

##### Example
    
    ...........
    ...##......
    .##..##.F..
    .#.....#...
    ...S..#....
    .....###...
    ...........

#### Image

A 24/32-bit .bmp file

*Allowed pixel values:*

If **"-I"** key is not used:
* **(255, 255, 255[, 255]) (white)** - empty space
* **(0, 0, 0[, 255]) (black)** - wall
* **Any other colour** -  start and end points (two pixels at most)

If **"-I"** key is used:

*Along with RGB vaues of a pixel, it's grayscale representation is used as well*

* **R, G or B is greater than 63 and other two components are 0** - start and end points (two pixels at most)
* **Greater than 63 (grayscale)** - wall
* **Less or equal to 63 (grayscale)** - empty space

### Usage

**solver [KEYS]**
    
Allowed keys:

        -p, --planar               set surface topology to planar
    
        -c, --cylinder             set surface topology to cylinder
                                   (allowed to jump from left border to right
                                   border and vice versa)
    
        -t, --tor                  set surface topology to tor
                                   (allowed to jump from left border to right
                                   border, form top to bottom and vice versa)
    
        -i, --in=IN_FILE_NAME      (required) read surface from IN_FILE_NAME
    
        -o, --out=OUT_FILE_NAME    write output to OUT_FILE_NAME
    
        -C, --convert              convert input form text to bmp or vice versa depending on
                                   file extensions
    
        -P, --path                 write path to "{OUT_FILE_NAME}.path.txt"
                                   (origin is top left corner)
                                   
        -I, --insensitive          treat wider range of colours as black/white
                                   (if input file is a .bmp image)
    
        -o, --out=OUT_FILE_NAME    write output to OUT_FILE_NAME (write to stdout by default)
    
        -h, --help                 display help (the one you're reading right now)
